import { ChainId } from 'eth-chains';

export const nullAddress = '0x0000000000000000000000000000000000000000';
export const apiKeyList = { INFURA_API_KEY: '84842078b09946638c03157f83405213' };
export const skipNonceChain: number[] = [ChainId.MeterMainnet, ChainId.MeterTestnet];
export const startNonceList: Record<number, bigint> = {
  [ChainId.GateChainMainnet]: 1n,
  [ChainId.KorthoMainnet]: 1n,
  [ChainId.IoTeXNetworkMainnet]: 1n,
  [ChainId.IoTeXNetworkTestnet]: 1n,
};
export const formatTypes = <const>['sighash', 'minimal', 'full', 'json'];
