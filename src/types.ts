export const fileExtensionTypes = <const>['yaml', 'yml', 'json'];
export type FileExtensionType = typeof fileExtensionTypes[number];

export type DecodeFunction = (text: string) => unknown;
export type EncodeFunction = (value: unknown) => string;
