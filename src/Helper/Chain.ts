import { chains } from 'eth-chains';

export const parseChain = async (identity: string | number) => {
  const name = String(identity);
  const key = Number(identity);
  const chain = Number.isSafeInteger(key) ? chains.getById(key) : chains.getByName(name);
  return undefined !== chain && 0 === Object.keys(chain).length ? undefined : chain;
};
