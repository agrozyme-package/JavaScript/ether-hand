import { consola } from 'consola';
import { Chain, ChainId, chains } from 'eth-chains';
import { Block, FetchRequest, getBigInt, getBytes, JsonRpcPayload, JsonRpcProvider, Network, Provider } from 'ethers';
import {
  count,
  Dictionary,
  filter,
  filterObject,
  forEachIndexed,
  fromPairs,
  keys,
  map,
  mapParallelAsync,
  mapToObject,
  wait,
} from 'rambdax';
import { $enum } from 'ts-enum-util';
import { getDefaultDuration, waitUntilAsync } from 'ts-retry';
import { apiKeyList, nullAddress, skipNonceChain, startNonceList } from '../Settings';
import { createGetCacheGlobal } from './Cache';

export const replaceApiKey = (text: string) => {
  let result = text;

  forEachIndexed((value, key) => {
    const search = '${' + key + '}';
    result = result.replace(search, value);
  }, apiKeyList);

  return result;
};

export const getRpcItems = (rawItems: string[]): string[] => {
  const items = map((item) => replaceApiKey(item), rawItems);
  return filter((item) => '' !== item && !item.includes('$'), items);
};

export const getRpcList = () => {
  const list = mapToObject<ChainId, Record<ChainId, string[]>>((key) => {
    const { rpc } = <Chain>chains.getById(key);
    return { [key]: getRpcItems(rpc) };
  }, getChainList());

  return <Record<ChainId, string[]>>filterObject((item) => 0 < item.length, list);
};

export const detectRpcUrl = async (rawItems: string[], httpsOnly: boolean = false, address: string = nullAddress) => {
  const items = httpsOnly ? filter((item) => item.startsWith('https://'), rawItems) : rawItems;

  if (0 === items.length) {
    return '';
  }

  const timeout = getDefaultDuration();
  //  const task = (url: string) => detectNetworkIdentity(url).then(() => url);
  //  const task = (url: string) => getProvider(url).then(() => url);
  const task = (url: string) => getAddressState(url, address).then(() => url);
  const tasks = map(task, items);

  try {
    return await waitUntilAsync(() => Promise.any(tasks), timeout).catch(() => '');
  } catch (error) {
    return '';
  }
};

export const detectRpcList = async (
  httpsOnly: boolean = false,
  verbose: boolean = false
): Promise<Dictionary<string>> => {
  const rpcList = getRpcList();

  const pairs = await mapParallelAsync<ChainId, [ChainId, string]>(async (key) => {
    const { name, rpc, chainId } = <Chain>chains.getById(key);
    const value = await detectRpcUrl(rpcList[key], httpsOnly, nullAddress);

    if (verbose) {
      if ('' === value) {
        consola.warn(`skip chain: ${name}`);
      } else {
        consola.success(`add chain: ${name}`);
      }
    }

    return [key, value];
  }, keys(rpcList));

  return fromPairs(filter(([key, value]) => '' !== value, pairs));
};

export const detectNetworkIdentity = async (url: string) => {
  const request = new FetchRequest(url);
  request.setHeader('content-type', 'application/json');
  //  request.timeout = getDefaultDuration();
  const payload: JsonRpcPayload = { id: Date.now(), method: 'eth_chainId', params: [], jsonrpc: '2.0' };
  request.body = JSON.stringify(payload);
  const response = await request.send();
  response.assertOk();
  const body = response.bodyJson;
  return getBigInt(body.result);
};

export const getAddressState = async (url: string, address: string) => {
  const provider = await getProvider(url);

  //  const balance = await provider.getBalance(address);
  //  const nonce = await provider.getTransactionCount(address).then((value) => BigInt(value));
  //  const code = await provider.getCode(address).then((value) => BigInt(getBytes(value).length));

  const [balance, nonce, code] = await Promise.all([
    provider.getBalance(address),
    provider.getTransactionCount(address).then((value) => BigInt(value)),
    provider.getCode(address).then((value) => BigInt(getBytes(value).length)),
    // provider.getLogs({ address, fromBlock: 0 }).then((value) => BigInt(value.length)),
  ]);

  // return { balance: 0n, nonce: 0n, code: 0n };
  return { balance, nonce, code };
};

export const checkProviders = async (rpcList: Dictionary<string>, address: string, verbose: boolean = false) => {
  const zero = 0n;

  const items = await mapParallelAsync(async (key: string) => {
    const { name, rpc, chainId } = <Chain>chains.getById(parseInt(key));
    const message = `check ${address}: ${name}`;
    const [data, error] = await wait(getAddressState(rpcList[key], address));

    if (error) {
      if (verbose) {
        //        console.dir(error);
        const data = <any>error;
        //      consola.error(data.toString());
        consola.error(`${message} (${data['code']}, ${data['requestBody']})`);
        //      consola.error(`${message} (${data['message']})`);
      }

      return null;
    }

    const { code, nonce, balance } = data;
    const startNonce = startNonceList[chainId] ?? zero;
    const checkNonce = skipNonceChain.includes(chainId) ? true : startNonce >= nonce;
    const result = zero < code ? true : checkNonce && zero === balance;

    if (verbose) {
      if (result) {
        // consola.success(message);
      } else {
        consola.info(message + ` (chainId=${chainId}, code=${code}, nonce=${nonce}, balance=${balance})`);
      }
    }

    return result;
  }, <string[]>keys(rpcList));

  const totalCount = items.length;
  const errorCount = count((item) => null === item, items);
  const successCount = count((item) => true === item, items);
  const failCount = count((item) => false === item, items);
  return { totalCount, errorCount, successCount, failCount };
};

export const getChainList = () => $enum(ChainId).getValues();

export const getProvider = async (url: string) => {
  try {
    const staticNetwork = Network.from(await detectNetworkIdentity(url));
    return new JsonRpcProvider(url, staticNetwork, { staticNetwork });
  } catch (error) {
    throw error;
  }
};

export const getBlockNumber = async (provider: Provider) =>
  await provider.getBlockNumber().then((value) => BigInt(value));

export const getBlock = async (provider: Provider, number: bigint, cache: boolean = true) => {
  const { chainId } = await provider.getNetwork();

  return await createGetCacheGlobal(
    `Chian.${chainId}.Block.${number}`,
    async () => {
      return <Block>await provider.getBlock(Number(number));
    },
    cache
  );
};
