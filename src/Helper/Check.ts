import { Dayjs } from 'dayjs';
import { Chain } from 'eth-chains';
import { isAddress } from 'ethers';
import { existsRpcFile } from './RpcList';
import { isRange } from './Validator';

export const checkAddress = (value: string) => {
  if (!isAddress(value)) {
    throw Error(`address is not valid: ${value}`);
  }
};

export const checkRpcListFile = () => {
  if (!existsRpcFile()) {
    throw Error(`rpc list cache file is not exists, use rpcList command to make it`);
  }
};

export const checkChain = (value: Chain | undefined) => {
  if (undefined === value) {
    throw Error(`chain is not valid`);
  }
};

export const checkUnsignedIntegerBits = (value: bigint, bits: bigint) => {
  const maximum = 2n ** bits;
  const minimum = 0n;

  if (!isRange(value, minimum, maximum)) {
    throw RangeError(`value (${value}) must be between 0 and 2^${bits} (${maximum})`);
  }
};

export const checkSignedIntegerBits = (value: bigint, bits: bigint) => {
  const exponent = bits - 1n;
  const maximum = 2n ** exponent;
  const minimum = -1n * maximum;

  if (!isRange(value, minimum, maximum)) {
    throw RangeError(`value (${value}) must be between 2^-${exponent} (${minimum}) and 2^${exponent} (${maximum})`);
  }
};

export const checkUint256 = (value: bigint) => {
  checkUnsignedIntegerBits(value, 256n);
};

export const checkInt256 = (value: bigint) => {
  checkSignedIntegerBits(value, 256n);
};

export const checkDayjs = (value: Dayjs) => {
  if (!value.isValid()) {
    throw Error(`value is not valid dayjs: ${value}`);
  }
};
