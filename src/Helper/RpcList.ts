import { consola } from 'consola';
import { existsSync } from 'fs';
import { join } from 'path';
import { Dictionary } from 'rambdax';
import { createGetCacheGlobal } from './Cache';
import { loadFile, saveFile } from './FileSystem';

export const getRpcFilePath = () => {
  return join(process.cwd(), '.rpcList.yaml');
};

export const existsRpcFile = () => {
  const path = getRpcFilePath();
  return existsSync(path);
};

export const writeRpcFile = async (items: Record<number, string>) => {
  const path = getRpcFilePath();
  await saveFile(path, items);
  consola.info(`write rpc file: ${path}`);
  return items;
};

export const readRpcFile = async (cache: boolean = true) => {
  return await createGetCacheGlobal(
    'rpcList',
    async () => {
      const path = getRpcFilePath();
      return await loadFile<Dictionary<string>>(path);
    },
    true
  );
};
