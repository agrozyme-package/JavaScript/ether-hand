import { FormatType, Interface, JsonFragment, JsonFragmentType } from 'ethers';

export type FragmentKey = keyof JsonFragment;
export type FragmentValue = JsonFragment[FragmentKey];
const normalizeFragment = (item: JsonFragment | string): JsonFragment | string => {
  if ('string' === typeof item) {
    return item;
  }

  const keys: Array<FragmentKey> = ['stateMutability', 'type'];

  const pairs = Object.entries({ ...item }).map(([key, value]) => {
    const data = 'string' === typeof value && keys.includes(<FragmentKey>key) ? value.toLowerCase() : value;
    return <[FragmentKey, FragmentValue]>[key, data];
  });

  return <JsonFragment>Object.fromEntries(pairs);
};

export const normalizeFragments = (items: Array<JsonFragment | string>) => {
  if (false === Array.isArray(items)) {
    return items;
  }

  return items.map((item) => normalizeFragment(item));
};

export const formatInterface = (data: Interface, format: FormatType) => {
  const items = data.fragments.map((item) => item.format(format));
  return 'json' === format ? items.map((item) => JSON.parse(item)) : items;
};

export const formatAbi = (items: Array<JsonFragment | string>, format: FormatType) => {
  const data = new Interface(normalizeFragments(items));
  return formatInterface(data, format);
};
