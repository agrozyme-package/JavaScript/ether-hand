import { Mutex } from 'async-mutex';

export type CacheKey = symbol | string;
export interface CacheStore extends Map<CacheKey, any> {}
export const cacheStore: CacheStore = new Map<CacheKey, any>();
const mutexStore = new Map<CacheKey, Mutex>();
const cacheKeyMutex = new Mutex();

export const createGetCache = async <T>(
  mutex: Mutex,
  cacheStore: CacheStore,
  cacheKey: CacheKey,
  callback: () => Promise<T>,
  cache: boolean = true
) => {
  return await mutex.runExclusive<T>(async () => {
    if (cache && cacheStore.has(cacheKey)) {
      return cacheStore.get(cacheKey);
    }

    const data = await callback();
    cacheStore.set(cacheKey, data);
    return data;
  });
};

export const createGetCacheGlobal = async <T>(
  cacheKey: CacheKey,
  callback: () => Promise<T>,
  cache: boolean = true
) => {
  const mutex = await cacheKeyMutex.runExclusive(async () => {
    if (!mutexStore.has(cacheKey)) {
      mutexStore.set(cacheKey, new Mutex());
    }

    return <Mutex>mutexStore.get(cacheKey);
  });

  return await createGetCache<T>(mutex, cacheStore, cacheKey, callback, cache);
};
