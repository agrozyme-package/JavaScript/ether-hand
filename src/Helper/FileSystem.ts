import { consola } from 'consola';
import { existsSync } from 'fs';
import { mkdir, readFile, writeFile } from 'fs/promises';
import json from 'json-bigint-native';
import { parse } from 'path';
import yaml from 'yaml';
import { DecodeFunction, EncodeFunction, FileExtensionType } from '../Types';

export const yamlEncode = (value: unknown) => {
  return yaml.stringify(value, { lineWidth: 0, singleQuote: true });
};

export const decodeList: Record<FileExtensionType, DecodeFunction> = {
  yaml: yaml.parse,
  yml: yaml.parse,
  json: json.parse,
};

export const encodeList: Record<FileExtensionType, EncodeFunction> = {
  yaml: yamlEncode,
  yml: yamlEncode,
  json: json.stringify,
};

export const getExtension = (path: string) => {
  return parse(path).ext.slice(1).toLowerCase();
};

export const loadFile = async <T>(path: string) => {
  const data = await readFile(path, { encoding: 'utf8' });
  const key = getExtension(path);

  if (!Object.hasOwn(decodeList, key)) {
    throw Error(`can not parse: ${path}`);
  }

  const value = <T>decodeList[<FileExtensionType>key](data);
  consola.info(`load file: ${path}`);
  return value;
};

export const saveFile = async <T>(path: string, value: T) => {
  const key = getExtension(path);
  const data = Object.hasOwn(encodeList, key) ? encodeList[<FileExtensionType>key](value) : '' + value;
  const { dir } = parse(path);

  if (!existsSync(dir)) {
    await mkdir(dir, { recursive: true });
  }

  await writeFile(path, data);
  consola.info(`save file: ${path}`);
};
