export const isRange = <T extends number | bigint>(value: T, minimum: T, maximum: T) => {
  return minimum <= value && maximum > value;
};
