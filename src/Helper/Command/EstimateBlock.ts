import { consola } from 'consola';
import { Dayjs } from 'dayjs';
import { Chain } from 'eth-chains';
import { Block, Provider } from 'ethers';
import { checkChain, checkDayjs, checkRpcListFile, checkUint256 } from '../Check';
import { getBlock, getBlockNumber, getProvider } from '../Provider';
import { readRpcFile } from '../RpcList';
import { isRange } from '../Validator';

export const getBlockTime = (fromBlock: Block, toBlock: Block) => {
  return (toBlock.timestamp - fromBlock.timestamp) / (toBlock.number - fromBlock.number);
};

export const getBlockList = async (provider: Provider, pastNumber: bigint = 0n) => {
  const lastNumber = await getBlockNumber(provider);
  const lastBlock = await getBlock(provider, lastNumber);
  const firstBlock = await getBlock(provider, 1n);
  const fromNumber = isRange(pastNumber, 1n, lastNumber) ? lastNumber - pastNumber : 1n;
  const fromBlock = await getBlock(provider, fromNumber);
  return { firstBlock, lastBlock, fromBlock };
};

export const findBlock = async (
  provider: Provider,
  specify: number,
  fromBlock: Block,
  toBlock: Block,
  previous: number = Number.NaN
): Promise<Block> => {
  const from = fromBlock.number;
  const to = toBlock.number;

  if (to === from) {
    return toBlock;
  }

  const middle = Math.ceil((to + from) / 2);
  const middleBlock = await getBlock(provider, BigInt(middle));
  const { timestamp } = middleBlock;
  console.dir({ from, to, previous, middle });

  if (previous === middle || timestamp === specify) {
    return middleBlock;
  }

  return timestamp > specify
    ? await findBlock(provider, specify, fromBlock, middleBlock, middle)
    : await findBlock(provider, specify, middleBlock, toBlock, middle);
};

export const estimateBlock = async (provider: Provider, specifyDateTime: Dayjs, pastNumber: bigint = 0n) => {
  const specifyTimestamp = specifyDateTime.unix();
  const { firstBlock, lastBlock, fromBlock } = await getBlockList(provider, pastNumber);
  const firstTimestamp = firstBlock.timestamp;
  const lastTimestamp = lastBlock.timestamp;
  const fromTimestamp = fromBlock.timestamp;

  if (specifyTimestamp < firstTimestamp) {
    return 0n;
  }

  if (specifyTimestamp === firstTimestamp) {
    return 1n;
  }

  if (specifyTimestamp === lastTimestamp) {
    return BigInt(lastBlock.number);
  }

  if (specifyTimestamp > lastTimestamp) {
    const interval = Math.ceil(Math.abs(specifyTimestamp - lastTimestamp) / getBlockTime(fromBlock, lastBlock));
    return BigInt(lastBlock.number) + BigInt(interval);
  }

  const block = await findBlock(provider, specifyTimestamp, firstBlock, lastBlock);
  return BigInt(block.number);
};

export const runEstimateBlock = async (chain: Chain, specify: Dayjs, past: bigint = 0n) => {
  checkChain(chain);
  checkDayjs(specify);
  checkUint256(past);
  checkRpcListFile();

  const rpcList = await readRpcFile();
  const rpc = rpcList[chain.chainId];
  const provider = await getProvider(rpc);
  const estimateNumber = await estimateBlock(provider, specify, past);
  const currentNumber = await getBlockNumber(provider);
  consola.success({ currentNumber, estimateNumber });
};
