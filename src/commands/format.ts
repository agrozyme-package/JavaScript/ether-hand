import { Flags } from '@oclif/core';
import consola from 'consola';
import { FormatType } from 'ethers';
import { readFile, realpath, writeFile } from 'fs/promises';
import { normalize, parse } from 'path';
//import { parse } from 'path/posix';
import { BaseCommand } from '../class/BaseCommand';
import { formatAbi } from '../Helper/Abi';
import { formatTypes } from '../Settings';

export default class Format extends BaseCommand {
  static description = 'format abi';

  static flags = {
    ...BaseCommand.flags,
    input: Flags.file({ required: true, description: 'input abi json file' }),
    type: Flags.string({ default: 'full', required: true, options: [...formatTypes] }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Format);
    const { verbose } = flags;

    if (verbose) {
      console.dir({ args, flags });
    }

    const type = <FormatType>flags.type;
    const input = await realpath(flags.input);
    const parsed = parse(input);
    const text = (await readFile(input)).toString();

    //    const abi = Interface.from(normalizeInterface(text));
    const data = formatAbi(JSON.parse(text), type);

    if (verbose) {
      consola.info(data);
    }

    const output = normalize(`${parsed.dir}/${parsed.name}.${type}${parsed.ext}`);
    await writeFile(output, JSON.stringify(data, null, 2));
    consola.info(`save file: ${output}`);
  }
}
