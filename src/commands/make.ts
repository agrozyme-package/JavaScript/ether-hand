import { Flags, ux } from '@oclif/core';
import { consola } from 'consola';
import { computeAddress, hexlify, randomBytes, SigningKey } from 'ethers';
import { filterObject, map, mapToObjectAsync } from 'rambdax';
import { BaseCommand } from '../class/BaseCommand';
import { checkProviders } from '../Helper/Provider';
import { existsRpcFile, readRpcFile } from '../Helper/RpcList';

export default class Make extends BaseCommand {
  static description = 'make address';

  static flags = {
    ...BaseCommand.flags,
    count: Flags.integer({ default: 1 }),
    check: Flags.boolean({ default: false }),
    compressed: Flags.boolean({ default: false }),
    verbose: Flags.boolean({ default: false }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Make);
    const { count, check, compressed, verbose } = flags;

    if (verbose) {
      console.dir({ args, flags });
    }

    if (!existsRpcFile()) {
      consola.warn(`cache file is not exists, use cache command to generate it.`);
      return;
    }

    const rpcList = await readRpcFile();
    const privateKeySet = new Set<string>();

    const makePrivateKey = () => hexlify(randomBytes(32));

    const getAddressList = map((privateKey: string) => ({
      privateKey,
      publicKey: SigningKey.computePublicKey(privateKey, compressed),
      address: computeAddress(privateKey),
    }));

    const checkTask = async (address: string) => {
      try {
        await checkProviders(rpcList, address, verbose);
        return true;
      } catch (error) {
        return false;
      }
    };

    for (let index = 0; index < count; index++) {
      let privateKey = makePrivateKey();

      while (privateKeySet.has(privateKey)) {
        privateKey = makePrivateKey();
      }

      privateKeySet.add(privateKey);
      // const phrase = Mnemonic.entropyToPhrase(privateKey);
      // console.dir(phrase);
    }

    const privateKeys = [...privateKeySet];
    console.dir(getAddressList(privateKeys));

    if (!check) {
      return;
    }

    const label = `check address`;
    console.time(label);
    // ux.action.start(label);
    const progress = ux.progress();
    progress.start(count, 0);

    const checked = await mapToObjectAsync<string, Record<string, boolean>>(async (key) => {
      const address = computeAddress(key);
      const value = await checkTask(address);
      progress.increment();
      return { [key]: value };
    }, privateKeys);

    progress.stop();
    // ux.action.stop();
    console.timeEnd(label);

    const addressList = getAddressList(Object.keys(filterObject((value, key) => value, checked)));
    console.dir(addressList);
    consola.info(`get ${addressList.length} clean address`);
  }
}
