import { Flags, ux } from '@oclif/core';
import { consola } from 'consola';
import { values } from 'rambdax';
import { BaseCommand } from '../class/BaseCommand';
import { detectRpcList, getChainList } from '../Helper/Provider';
import { writeRpcFile } from '../Helper/RpcList';

export default class RpcList extends BaseCommand {
  static description = 'make rpc list cache file';

  static flags = {
    ...BaseCommand.flags,
    verbose: Flags.boolean({ default: false }),
    httpsOnly: Flags.boolean({ default: false }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(RpcList);
    const { verbose, httpsOnly } = flags;

    if (verbose) {
      console.dir({ args, flags });
    }

    const label = `detect chains`;
    consola.info(`start: ${label}`);
    console.time(label);
    ux.action.start(label);
    const items = await detectRpcList(httpsOnly, verbose);
    ux.action.stop();
    console.timeEnd(label);
    consola.info(`stop: ${label}`);

    await writeRpcFile(items);
    const count = values(items).length;
    const total = getChainList().length;
    consola.info(`live chains: ${count} / ${total}`);
    // this.exit();
  }
}
