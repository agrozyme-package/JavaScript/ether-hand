import { Flags } from '@oclif/core';
import { computeAddress, SigningKey } from 'ethers';
import { map } from 'rambdax';
import { BaseCommand } from '../class/BaseCommand';

export default class Address extends BaseCommand {
  static description = 'compute address';

  static flags = {
    ...BaseCommand.flags,
    privateKey: Flags.string({ multiple: true, required: true }),
    compressed: Flags.boolean({ default: false }),
    verbose: Flags.boolean({ default: false }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Address);
    const { privateKey: keys, compressed, verbose } = flags;

    if (verbose) {
      console.dir({ args, flags });
    }

    const data = map((privateKey) => {
      return {
        privateKey,
        publicKey: SigningKey.computePublicKey(privateKey, compressed),
        address: computeAddress(privateKey),
      };
    }, keys);

    console.dir(data);
    // this.exit();
  }
}
