import { Flags } from '@oclif/core';
import { consola } from 'consola';
import { BaseCommand } from '../class/BaseCommand';
import { parseChain } from '../Helper/Chain';

export default class Info extends BaseCommand {
  static description = 'get chain information';

  static flags = {
    ...BaseCommand.flags,
    chain: Flags.string({ required: true, default: '' }),
    verbose: Flags.boolean({ default: false }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Info);
    const { verbose } = flags;

    if (verbose) {
      console.dir({ args, flags });
    }

    const chain = await parseChain(flags.chain);

    if (chain) {
      console.dir(chain);
    } else {
      consola.info(`no find chain data`);
    }
  }
}
