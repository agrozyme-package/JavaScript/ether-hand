import { rawChainData } from 'eth-chainlist';
import { chains } from 'eth-chains';
import { BaseCommand } from '../class/BaseCommand';

export default class Test extends BaseCommand {
  static description = 'describe the command here';

  static flags = {
    ...BaseCommand.flags,
  };

  static args = {};

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Test);
    const ethChains = Object.keys(chains.all()).length;
    const chainDatas = Object.keys(rawChainData()).length;
    console.dir({ ethChains, chainDatas });
  }
}
