import { Flags } from '@oclif/core';
import dayjs from 'dayjs';
import { Chain } from 'eth-chains';
import { BaseCommand } from '../class/BaseCommand';
import { parseChain } from '../Helper/Chain';
import { runEstimateBlock } from '../Helper/Command/EstimateBlock';

export default class EstimateBlock extends BaseCommand {
  static description = 'estimate block number by datetime';

  static flags = {
    ...BaseCommand.flags,
    chain: Flags.string({ required: true, default: '' }),
    specify: Flags.string({
      required: true,
      description: `ISO 8601 specify time (ex: ${dayjs().toISOString()})`,
    }),
    past: Flags.integer({ default: 0, description: 'use last blocks time' }),
    verbose: Flags.boolean({ default: false }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(EstimateBlock);

    const chain = <Chain>await parseChain(flags.chain);
    const specify = dayjs(flags.specify);
    const past = BigInt(flags.past);

    await runEstimateBlock(chain, specify, past);
  }
}
