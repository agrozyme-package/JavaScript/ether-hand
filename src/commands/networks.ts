import { Flags } from '@oclif/core';
import { consola } from 'consola';
import { Chain, ChainId, chains } from 'eth-chains';
import { anyTrue, Dictionary, filterObject, keys, map, mapToObject } from 'rambdax';
import { BaseCommand } from '../class/BaseCommand';
import { parseChain } from '../Helper/Chain';
import { getChainList } from '../Helper/Provider';

export default class Networks extends BaseCommand {
  static description = 'check chains that need api key';

  static flags = {
    ...BaseCommand.flags,
    chain: Flags.string({ default: '' }),
    verbose: Flags.boolean({ default: false }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Networks);
    const { chain, verbose } = flags;

    if (verbose) {
      console.dir({ args, flags });
    }

    const chainList = getChainList();

    // const list = compose(
    //   filterObject((value) => 0 < value.length),
    //   mapToObject<ChainId, Dictionary<string[]>>((key) => {
    //     const { name, rpc } = <Chain>chains.getById(key);
    //     const test = map((item) => item.includes('$'), rpc);
    //     const value = anyTrue(...test) ? rpc : [];
    //     return { [name]: value };
    //   })
    // )(chainList);
    // console.dir(list);

    const rpcList = filterObject(
      (value) => 0 < value.length,
      mapToObject<ChainId, Dictionary<string[]>>((key) => {
        const { name, rpc } = <Chain>chains.getById(key);
        const test = map((item) => item.includes('$'), rpc);
        const value = anyTrue(...test) ? rpc : [];
        return { [name]: value };
      }, chainList)
    );

    console.dir(rpcList);

    if ('' !== chain) {
      const item = await parseChain(chain);

      if (item) {
        console.dir(item);
      } else {
        consola.info(`no find ${chain} chain data`);
      }
    }

    consola.info(`networks: ${keys(rpcList).length}`);
    // this.exit();
  }
}
