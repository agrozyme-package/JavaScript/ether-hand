import { Flags } from '@oclif/core';
import { BaseCommand } from '../class/BaseCommand';
import { getAddressState } from '../Helper/Provider';

export default class Local extends BaseCommand {
  static description = 'check local network address';

  static flags = {
    ...BaseCommand.flags,
    port: Flags.integer({ default: 8545 }),
    address: Flags.string({ required: true }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Local);
    const { port, address } = flags;
    console.dir({ args, flags });

    const url = `http://127.0.0.1:${port}`;
    const { code, nonce, balance } = await getAddressState(url, address);
    console.dir({ code, nonce, balance });
  }
}
