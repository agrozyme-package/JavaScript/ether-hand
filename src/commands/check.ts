import { Flags, ux } from '@oclif/core';
import { consola } from 'consola';
import { BaseCommand } from '../class/BaseCommand';
import { checkAddress, checkRpcListFile } from '../Helper/Check';
import { checkProviders } from '../Helper/Provider';
import { readRpcFile } from '../Helper/RpcList';

export default class Check extends BaseCommand {
  static description = 'check address';

  static flags = {
    ...BaseCommand.flags,
    address: Flags.string({ required: true }),
    verbose: Flags.boolean({ default: false }),
  };

  public async run(): Promise<void> {
    const { args, flags } = await this.parse(Check);
    const { address, verbose } = flags;

    if (verbose) {
      console.dir({ args, flags });
    }

    checkAddress(address);
    checkRpcListFile();
    const rpcList = await readRpcFile();

    const label = `check chains`;
    console.time(label);
    ux.action.start(label);
    const { totalCount, errorCount, successCount, failCount } = await checkProviders(rpcList, address, verbose);
    ux.action.stop();
    console.timeEnd(label);

    if (verbose) {
      console.dir({ totalCount, errorCount, successCount, failCount });
    } else {
      consola.info(`error count: ${errorCount}`);
    }

    if (0 === failCount) {
      consola.success(`${address} is a clean address.`);
    } else {
      consola.warn(`${address} is not a clean address.`);
    }
  }
}
