oclif-hello-world
=================

oclif example Hello World CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/oclif-hello-world.svg)](https://npmjs.org/package/oclif-hello-world)
[![CircleCI](https://circleci.com/gh/oclif/hello-world/tree/main.svg?style=shield)](https://circleci.com/gh/oclif/hello-world/tree/main)
[![Downloads/week](https://img.shields.io/npm/dw/oclif-hello-world.svg)](https://npmjs.org/package/oclif-hello-world)
[![License](https://img.shields.io/npm/l/oclif-hello-world.svg)](https://github.com/oclif/hello-world/blob/main/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g ether-hand
$ ether-hand COMMAND
running command...
$ ether-hand (--version)
ether-hand/0.0.0 win32-x64 node-v20.12.2
$ ether-hand --help [COMMAND]
USAGE
  $ ether-hand COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`ether-hand Address`](#ether-hand-address)
* [`ether-hand Check`](#ether-hand-check)
* [`ether-hand EstimateBlock`](#ether-hand-estimateblock)
* [`ether-hand Format`](#ether-hand-format)
* [`ether-hand Info`](#ether-hand-info)
* [`ether-hand Local`](#ether-hand-local)
* [`ether-hand Make`](#ether-hand-make)
* [`ether-hand Networks`](#ether-hand-networks)
* [`ether-hand RpcList`](#ether-hand-rpclist)
* [`ether-hand Test`](#ether-hand-test)
* [`ether-hand help [COMMAND]`](#ether-hand-help-command)

## `ether-hand Address`

compute address

```
USAGE
  $ ether-hand Address --privateKey <value> [--debug] [--compressed] [--verbose]

FLAGS
  --compressed
  --debug                  show error stack
  --privateKey=<value>...  (required)
  --verbose

DESCRIPTION
  compute address

EXAMPLES
  $ ether-hand Address
```

_See code: [src/commands/Address.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Address.ts)_

## `ether-hand Check`

check address

```
USAGE
  $ ether-hand Check --address <value> [--debug] [--verbose]

FLAGS
  --address=<value>  (required)
  --debug            show error stack
  --verbose

DESCRIPTION
  check address

EXAMPLES
  $ ether-hand Check
```

_See code: [src/commands/Check.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Check.ts)_

## `ether-hand EstimateBlock`

estimate block number by datetime

```
USAGE
  $ ether-hand EstimateBlock --chain <value> --specify <value> [--debug] [--past <value>] [--verbose]

FLAGS
  --chain=<value>    (required)
  --debug            show error stack
  --past=<value>     use last blocks time
  --specify=<value>  (required) ISO 8601 specify time (ex: 2024-04-17T15:38:25.758Z)
  --verbose

DESCRIPTION
  estimate block number by datetime

EXAMPLES
  $ ether-hand EstimateBlock
```

_See code: [src/commands/EstimateBlock.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/EstimateBlock.ts)_

## `ether-hand Format`

format abi

```
USAGE
  $ ether-hand Format --input <value> --type sighash|minimal|full|json [--debug]

FLAGS
  --debug          show error stack
  --input=<value>  (required) input abi json file
  --type=<option>  (required) [default: full]
                   <options: sighash|minimal|full|json>

DESCRIPTION
  format abi

EXAMPLES
  $ ether-hand Format
```

_See code: [src/commands/Format.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Format.ts)_

## `ether-hand Info`

get chain information

```
USAGE
  $ ether-hand Info --chain <value> [--debug] [--verbose]

FLAGS
  --chain=<value>  (required)
  --debug          show error stack
  --verbose

DESCRIPTION
  get chain information

EXAMPLES
  $ ether-hand Info
```

_See code: [src/commands/Info.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Info.ts)_

## `ether-hand Local`

check local network address

```
USAGE
  $ ether-hand Local --address <value> [--debug] [--port <value>]

FLAGS
  --address=<value>  (required)
  --debug            show error stack
  --port=<value>     [default: 8545]

DESCRIPTION
  check local network address

EXAMPLES
  $ ether-hand Local
```

_See code: [src/commands/Local.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Local.ts)_

## `ether-hand Make`

make address

```
USAGE
  $ ether-hand Make [--debug] [--count <value>] [--check] [--compressed] [--verbose]

FLAGS
  --check
  --compressed
  --count=<value>  [default: 1]
  --debug          show error stack
  --verbose

DESCRIPTION
  make address

EXAMPLES
  $ ether-hand Make
```

_See code: [src/commands/Make.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Make.ts)_

## `ether-hand Networks`

check chains that need api key

```
USAGE
  $ ether-hand Networks [--debug] [--chain <value>] [--verbose]

FLAGS
  --chain=<value>
  --debug          show error stack
  --verbose

DESCRIPTION
  check chains that need api key

EXAMPLES
  $ ether-hand Networks
```

_See code: [src/commands/Networks.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Networks.ts)_

## `ether-hand RpcList`

make rpc list cache file

```
USAGE
  $ ether-hand RpcList [--debug] [--verbose] [--httpsOnly]

FLAGS
  --debug      show error stack
  --httpsOnly
  --verbose

DESCRIPTION
  make rpc list cache file

EXAMPLES
  $ ether-hand RpcList
```

_See code: [src/commands/RpcList.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/RpcList.ts)_

## `ether-hand Test`

describe the command here

```
USAGE
  $ ether-hand Test [--debug]

FLAGS
  --debug  show error stack

DESCRIPTION
  describe the command here

EXAMPLES
  $ ether-hand Test
```

_See code: [src/commands/Test.ts](https://github.com/agrozyme/ether-hand/blob/v0.0.0/src/commands/Test.ts)_

## `ether-hand help [COMMAND]`

Display help for ether-hand.

```
USAGE
  $ ether-hand help [COMMAND...] [-n]

ARGUMENTS
  COMMAND...  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for ether-hand.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v6.0.21/src/commands/help.ts)_
<!-- commandsstop -->
